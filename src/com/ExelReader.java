package com;

import com.company.Hotel;
import com.company.Room;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ExelReader {

    public static List<Hotel> hotels = new ArrayList<>();
    public static List<Room> rooms = new ArrayList<>();

    public static void reader(String filepath) throws IOException, InvalidFormatException {

        // Creating a Workbook from an Excel file (xlsx)
        Workbook workbook = WorkbookFactory.create(new File(filepath));
        // Getting the Sheet at index zero
        Sheet hotelSheet = workbook.getSheetAt(0);
        Iterator<Row> rowItr = hotelSheet.iterator();

        while (rowItr.hasNext()) {
            Row row = rowItr.next();
            // First row is header so skip it
            if (row.getRowNum() == 0) {
                continue;
            }
            String name = row.getCell(1) != null ? row.getCell(1).toString() : " ";
            String city = row.getCell(2) != null ? row.getCell(2).toString() : " ";
            String country = row.getCell(3) != null ? row.getCell(3).toString() : " ";
            int stars = row.getCell(4) != null ? (int) row.getCell(4).getNumericCellValue() : 0;
            int id = row.getCell(0) != null ? (int) row.getCell(0).getNumericCellValue() : 0;
            Hotel hotel = new Hotel(name, city, country, stars, id);
            hotels.add(hotel);

            workbook.close();
        }

        Workbook workbook1 = WorkbookFactory.create(new File(filepath));
        Sheet roomSheet = workbook1.getSheetAt(1);
        Iterator<Row> rowItr1 = roomSheet.iterator();
        Filter filter = new Filter();

        while (rowItr1.hasNext()) {
            Row row1 = rowItr1.next();
            // First row is header so skip it
            if (row1.getRowNum() == 0) {
                continue;
            }
            int id= row1.getCell(1) != null ? (int)row1.getCell(0).getNumericCellValue() : 0;
            double price = row1.getCell(2) != null ? row1.getCell(2).getNumericCellValue() : 0;
            int numOfRooms = row1.getCell(3) != null ? (int) row1.getCell(3).getNumericCellValue() : 0;

            Room room = new Room(id, numOfRooms, price);
            rooms.add(room);
        }
        workbook1.close();


    }


}



