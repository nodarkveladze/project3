package com;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import java.io.IOException;


public class Main extends Application {

    public static void main(String[] args) throws IOException, InvalidFormatException {
        launch(args);
        ExelReader.reader("C:\\Users\\User\\Desktop\\Hotels.xlsx");

    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("sample.fxml"));
        root.getStylesheets().add(getClass().getResource("../Style/style.css").toExternalForm());
        Image icon = new Image(Filter.class.getResource("../images/hotel.jpg").toExternalForm(), false);
        primaryStage.getIcons().add(icon);
        primaryStage.setTitle("hotel filter");
        primaryStage.setScene(new Scene(root, 650, 400));
        primaryStage.setResizable(false);
        primaryStage.show();
    }

}

