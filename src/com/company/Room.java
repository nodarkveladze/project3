package com.company;

public class Room {
    private int id;
    private int numOfRooms;
    private double price;

    public Room(int id, int numOfRooms, double price) {
        this.id = id;
        this.numOfRooms = numOfRooms;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumOfRooms() {
        return numOfRooms;
    }

    public void setNumOfRooms(int numOfRooms) {
        this.numOfRooms = numOfRooms;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String toString() {
        return this.id + " " + this.numOfRooms + " " + this.price;
    }
}
